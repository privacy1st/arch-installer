function validate_args(){
  local num_args="${1}"
  shift

  if [ "$#" -ne "${num_args}" ]; then
    printf '%s\n' "ERROR: ${num_args} arguments required"
    return 1
  fi
  for i in "$@"; do
    if [ -z "${i}" ]; then
      printf '%s\n' 'ERROR: All given args must not be empty'
      return 1
    fi
  done
}
