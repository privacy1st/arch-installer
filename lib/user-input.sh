function ask_user_if_empty {
  # If variable named $1 is empty, then ask for user input.
  #
  # One line of user input is read.
  # The user input must not be empty.
  #
  # arg $1: Name of variable to store user input
  # arg $2: Text to display (e.g. "Enter hostname:")
  validate_args 2 "$@" || return $?
  local -n user_input_ptr=$1
  local display_text="${2}"

  # If user_input_ptr is not empty, return
  if [ -n "${user_input_ptr:-}" ]; then
    return 0
  fi
  # Else, ask for user input!

  printf '%s\n' "${display_text}"
  read -r user_input_ptr || return $?

  # User input must not be empty
  if [ -z "${user_input_ptr}" ]; then
    echo 'The input must not be empty!'
    return 1
  fi
}

function single_choice_if_empty {
  # If variable named $1 is empty,
  # then let user select one of the given options.
  #
  # arg $1: Name of variable to store the selected option-name
  # arg $2: Text to display
  # arg $3: Name of variable with array of options to display (for each option there must be two entries in the array: name and description)
  validate_args 3 "$@" || return $?
  local -n selected_option_ptr=$1
  local display_text="${2}"
  local -n menu_options_ptr=$3

  # If selected_option_ptr is not empty, return.
  if [ -n "${selected_option_ptr:-}" ]; then
    return 0
  fi
  # Else, ask for user input!

  selected_option_ptr=$(dialog --stdout --menu "${display_text}" 0 0 0 "${menu_options_ptr[@]}") || {
    echo 'Error during menu selection!'
    return 1
  }
  clear
}

function multi_choice_if_empty {
  # If variable named $1 is empty,
  # then let user select one or more of the given options.
  #
  # arg $1: Name of variable to store the selected option-names as array
  # arg $2: Text to display
  # arg $3: Name of variable with array of options to display (for each option there must be three entries in the array: name, description, on/off)
  validate_args 3 "$@" || return $?
  local -n selected_option_ptr=$1
  local display_text="${2}"
  local -n menu_options_ptr=$3

  # If selected_option_ptr is not empty, return.
  if [ -n "${selected_option_ptr:-}" ]; then
    return 0
  fi
  # Else, ask for user input!

  selected_option_ptr=$(dialog --stdout --checklist "${display_text}" 0 0 0 "${menu_options_ptr[@]}") || {
    echo 'Error during menu selection!'
    return 1
  }
  clear

  # Result of dialog is a space separated list.
  # Store this as an array.
  # Without newline at last array element: https://unix.stackexchange.com/a/519917/315162
  # readarray -d " " -t ptr < <(printf '%s' "selected_option_ptr")
  #
  space_separated_to_array selected_option_ptr selected_option_ptr || return $?
}
