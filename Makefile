.PHONY: all
all: test

test:
	shellcheck --check-sourced --external-sources de-p1st-installer.sh

SINGLE_FILE_CFG := example-vbox.cfg
SINGLE_FILE_DST := example-vbox-binary.sh
single-file:  # Creates a single, executable .sh file a given configuration file.
	py-replace --text 'main "' --repl '# main "' --count 1 \
		< de-p1st-installer.sh \
		> single-file-temp.sh
	chmod +x single-file-temp.sh
	bash -v ./single-file-temp.sh example-vbox.cfg 2>&1 | \
		py-regex-replace --pattern '^\s*source\s+.+$$' --repl ': # sourced file included below:' --count '-1' | \
		py-replace --text '# main "' --repl 'main "' --count 1 \
		> ${SINGLE_FILE_DST}
	rm single-file-temp.sh
	chmod +x ${SINGLE_FILE_DST}
